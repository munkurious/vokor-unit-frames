//Vokor Unit Frames
//By Vokor 
// - Munkurious : Lead Developer
// - Iban : Code for Health Bar
// - Ephi : Code for avatar in Object View
// - FAMFAMFAM.com : Icons by Mark James - Thanks!

//client.cs

//font
//if you have TBP's Custom Chat add-on, it will use that font! if not, Verdana

if(isFile("config/client/chat/vars.cs"))
{
	exec("config/client/chat/vars.cs");
	$vUF::font = $Chat::FontFamily;
}
else
{
	$vUF::font = "Verdana";
}

//profiles
new GuiControlProfile(vUF_nameTextProfile)
{
	fontColor = "255 255 255 255";
	fontSize = 16;
	fontType = $vUF::font;
	justify = "Center";  
};

$vUF::topper = 0;

//gui Registering

function clientCmdvUF_registerOne()
{
	commandToServer( 'vUF_registerTwo' , 1 );
}


//adding unit Frames

function clientCmdvUF_pushUF( %this , %name )
{
	if( %this $= "SELF" )
	{
		if(isObject(vUF_main_self))
		{
			vUF_main_self.delete();
		}

		%a = new GuiSwatchCtrl(vUF_main_self) {
			profile = "GuiDefaultProfile";
			horizSizing = "right";
			vertSizing = "bottom";
			position = (getWord(getRes(),0)/2)-275 SPC getWord(getRes(),1)-170+60;
			extent = "200 80";
			minExtent = "8 2";
			enabled = "1";
			visible = "1";
			clipToParent = "1";
			color = "0 0 0 120";

			new GuiSwatchCtrl(vUF_avBG_self) {
				profile = "GuiDefaultProfile";
				horizSizing = "right";
				vertSizing = "bottom";
				position = "5 5";
				extent = "68 68";
				minExtent = "8 2";
				enabled = "1";
				visible = "1";
				clipToParent = "1";
				color = "0 0 0 200";
	
				new GuiObjectView(vUF_AV_self) {
			 		profile = "GuiDefaultProfile";
			 		horizSizing = "right";
			 		vertSizing = "bottom";
			 		position = "2 2";
			 		extent = "64 176";
			 		minExtent = "8 2";
			 		enabled = "1";
			 		visible = "1";
			 		clipToParent = "1";
			 		cameraZRot = "0";
			 		forceFOV = "0";
			   };

				new guiBitMapCtrl(vUF_deathImage_self)
				{
					profile = "GuiDefaultProfile";
					horizSizing = "right";
					vertSizing = "bottom";
					position = "2 2";
					extent = "64 64";
					minExtent = "8 2";
					enabled = "1";
					visible = "0";
					clipToParent = "1";
					bitmap = "base/client/ui/CI/skull.png";
					wrap = "0";
					lockAspectRatio = "0";
					alignLeft = "0";
					alignTop = "0";
					overflowImage = "0";
					keepCached = "0";
				};
			};
	
			new GuiSwatchCtrl(vUF_nameBG_self) {
				profile = "GuiDefaultProfile";
				horizSizing = "right";
				vertSizing = "bottom";
				position = "80 7";
				extent = "110 20";
				minExtent = "8 2";
				enabled = "1";
				visible = "1";
				clipToParent = "1";
				color = "0 0 0 200";

				new GuiTextCtrl(vUF_name_self) {
					profile = "vUF_nameTextProfile";
					horizSizing = "right";
					vertSizing = "bottom";
					position = "0 0";
					extent = "110 20";
					minExtent = "8 2";
					enabled = "1";
					visible = "1";
					clipToParent = "1";
					text = %name;
					maxLength = "255";
				};
			};

			new guiBitMapCtrl(vUF_heartIcon_self)
			{
					profile = "GuiDefaultProfile";
					horizSizing = "right";
					vertSizing = "bottom";
					position = "80 30";
					extent = "16 16";
					minExtent = "8 2";
					enabled = "1";
					visible = "1";
					clipToParent = "1";
					bitmap = "Add-Ons/System_vUF/icons/heart.png";
					wrap = "0";
					lockAspectRatio = "0";
					alignLeft = "0";
					alignTop = "0";
					overflowImage = "0";
					keepCached = "0";
			};
	
			new GuiSwatchCtrl(vUF_healthbar_self) {
				profile = "GuiDefaultProfile";
				horizSizing = "left";
				vertSizing = "bottom";
				position = "98 30"; //80, 30, 110, 15
				extent = "92 15";
				minExtent = "92 9";
				visible = "1";
				color = "0 0 0 255";
	
				new GuiSwatchCtrl(vUF_healthMeter_self) {
					profile = "GuiDefaultProfile";
					horizSizing = "right";
					vertSizing = "bottom";
					position = "1 1";
					extent = "90 13";
					minExtent = "1 7";
					visible = "1";
					color = "0 255 0 255";
				};
			};

			new guiBitMapCtrl(vUF_energyIcon_self)
			{
					profile = "GuiDefaultProfile";
					horizSizing = "right";
					vertSizing = "bottom";
					position = "80 50";
					extent = "16 16";
					minExtent = "8 2";
					enabled = "1";
					visible = "1";
					clipToParent = "1";
					bitmap = "Add-Ons/System_vUF/icons/lightning.png";
					wrap = "0";
					lockAspectRatio = "0";
					alignLeft = "0";
					alignTop = "0";
					overflowImage = "0";
					keepCached = "0";
			};
			
			new GuiHealthBarHud(vUF_energy_self) {
				profile = "GuiDefaultProfile";
				horizSizing = "right";
				vertSizing = "bottom";
				position = "98 50";
				extent = "92 15";
				minExtent = "8 2";
				enabled = "1";
				visible = "1";
				clipToParent = "1";
				fillColor = "0.000000 0.000000 0.000000 0.500000";
				frameColor = "0.000000 0.000000 0.000000 1.000000";
				damageFillColor = "0.000000 0.000000 1.000000 1.000000";
				pulseRate = "0";
				pulseThreshold = "0.3";
				flipped = "0";
				showFill = "1";
				showFrame = "1";
				displayEnergy = "1";
			};
		};

		playGui.add(%a);
		playGui.remove(HUD_EnergyBar);

		//avatar stuff - thanks RTB!

		vUF_AV_self.setEmpty();
		vUF_AV_self.dynamicObject = "fffObject";
		vUF_AV_self.setObject(vUF_AV_self.dynamicObject, "base/data/shapes/player/m.dts", "", 0);

		vUF_AV_self.setSequence(vUF_AV_self.dynamicObject, 0, "headup", 0);
		vUF_AV_self.setSequence(vUF_AV_self.dynamicObject, 1, "run", 0.85);
		   
		vUF_AV_self.forceFOV = 15;
		vUF_AV_self.setOrbitDist(3);
		vUF_AV_self.setCameraRot(0.22,0.5,3.14);
		vUF_AV_self.lightDirection = "0 0.2 0.2";

		%face = $Pref::Avatar::FaceName;
		%skincolor = $Pref::Avatar::HeadColor;
		%hat = $Pref::Avatar::Hat;
		%hatColor = $Pref::Avatar::HatColor;
		%accent = $Pref::Avatar::Accent;
		%accentColor = $Pref::Avatar::AccentColor;
		%chest = $Pref::Avatar::Chest;
		%chestColor = $Pref::Avatar::TorsoColor;
		%arm = $Pref::Avatar::LArm;
		%armColor = $pref::Avatar::LArmColor;
		%chestDecal = $Pref::Avatar::DecalName;
		%pack = $pref::Avatar::Pack;
		%packColor = $pref::Avatar::PackColor;
		%secondPack = $Pref::Avatar::SecondPack;
		%secondPackColor = $pref::Avatar::SecondPackColor;
		
		%i=0;
		while($face[%i] !$= "")
		{
		 	if($face[%i] $= %face)
		 	{
		 		%face = %i;
		 		break;
		 	}
		 	%i++;
		 	
		 	if(%i > 500)
		 		break;
		}
		
		%i=0;
		while($decal[%i] !$= "")
		{
			if($decal[%i] $= %chestDecal)
			{
			 	%chestDecal = %i;
			 	break;
			}
			%i++;
			
			if(%i > 500)
				break;
		}

		%parts = "accent hat chest pack secondpack larm rarm lhand rhand hip lleg rleg";
		for(%i=0;%i<getWordCount(%parts);%i++)
		{
			%k = 0;
			eval("%partName = $"@getWord(%parts,%i)@%k@";");
			while(%partName !$= "")
			{
				if(%partName !$= "none")
					vUF_AV_self.hidenode(vUF_AV_self.dynamicObject,%partName);
				eval("%partName = $"@getWord(%parts,%i)@%k++@";");
				%b++;
			}
		}
		
		vUF_AV_self.setNodeColor(vUF_AV_self.dynamicObject,"ALL",%skinColor);
		vUF_AV_self.setIFLFrame(vUF_AV_self.dynamicObject,"face",%face);
		vUF_AV_self.setIFLFrame(vUF_AV_self.dynamicObject,"decal",%chestDecal);
		
		if(%hat !$= "" && %hat !$= 0)
		{
			vUF_AV_self.unhidenode(vUF_AV_self.dynamicObject,$hat[%hat]);
			vUF_AV_self.setnodeColor(vUF_AV_self.dynamicObject,$hat[%hat],%hatColor);
			
			%accent = getWord($accentsAllowed[$hat[%hat]],%accent);
			if(%accent !$= "" && %accent !$= "none")
			{
				vUF_AV_self.unhidenode(vUF_AV_self.dynamicObject,%accent);
				vUF_AV_self.setnodeColor(vUF_AV_self.dynamicObject,%accent,%accentColor);
			}
		}
		
		if(%pack !$= "" && %pack !$= 0)
		{
			vUF_AV_self.unhidenode(vUF_AV_self.dynamicObject,$pack[%pack]);
			vUF_AV_self.setnodeColor(vUF_AV_self.dynamicObject,$pack[%pack],%packColor);
		}

		if(%secondpack !$= "" && %secondpack !$= 0)
		{
			vUF_AV_self.unhidenode(vUF_AV_self.dynamicObject,$secondpack[%secondpack]);
			vUF_AV_self.setnodeColor(vUF_AV_self.dynamicObject,$secondpack[%secondpack],%secondpackColor);
		}

		vUF_AV_self.unhidenode(vUF_AV_self.dynamicObject,$chest[%chest]);
		vUF_AV_self.setNodeColor(vUF_AV_self.dynamicObject,$chest[%chest],%chestColor);
		vUF_AV_self.unhidenode(vUF_AV_self.dynamicObject,$larm[%arm]);
		vUF_AV_self.setNodeColor(vUF_AV_self.dynamicObject,$larm[%arm],%armColor);
		vUF_AV_self.unhidenode(vUF_AV_self.dynamicObject,$rarm[%arm]);
		vUF_AV_self.setNodeColor(vUF_AV_self.dynamicObject,$rarm[%arm],%armColor);

		vUF_AV_self.hideNode(vUF_AV_self.dynamicObject,"lhand");
		vUF_AV_self.hideNode(vUF_AV_self.dynamicObject,"rhand");

		//start loop for getting info
		clientCmdvUF_infoLoop();
	}
	else if( %this $= "target" )
	{
		if(isObject(vUF_main_2))
		{
			vUF_main_2.delete();
		}

		%a = new GuiSwatchCtrl(vUF_main_2) {
			profile = "GuiDefaultProfile";
			horizSizing = "right";
			vertSizing = "bottom";
			position = (getWord(getRes(),0)/2)+75 SPC getWord(getRes(),1)-170+60;
			extent = "200 80";
			minExtent = "8 2";
			enabled = "1";
			visible = "1";
			clipToParent = "1";
			color = "0 0 0 120";

			new GuiSwatchCtrl(vUF_avBG_2) {
				profile = "GuiDefaultProfile";
				horizSizing = "right";
				vertSizing = "bottom";
				position = "5 5";
				extent = "68 68";
				minExtent = "8 2";
				enabled = "1";
				visible = "1";
				clipToParent = "1";
				color = "0 0 0 200";
	
				new GuiObjectView(vUF_AV_2) {
			 		profile = "GuiDefaultProfile";
			 		horizSizing = "right";
			 		vertSizing = "bottom";
			 		position = "2 2";
			 		extent = "64 176";
			 		minExtent = "8 2";
			 		enabled = "1";
			 		visible = "1";
			 		clipToParent = "1";
			 		cameraZRot = "0";
			 		forceFOV = "0";
			   };

				new guiBitMapCtrl(vUF_deathImage_2)
				{
					profile = "GuiDefaultProfile";
					horizSizing = "right";
					vertSizing = "bottom";
					position = "2 2";
					extent = "64 64";
					minExtent = "8 2";
					enabled = "1";
					visible = "0";
					clipToParent = "1";
					bitmap = "base/client/ui/CI/skull.png";
					wrap = "0";
					lockAspectRatio = "0";
					alignLeft = "0";
					alignTop = "0";
					overflowImage = "0";
					keepCached = "0";
				};
			};
	
			new GuiSwatchCtrl(vUF_nameBG_2) {
				profile = "GuiDefaultProfile";
				horizSizing = "right";
				vertSizing = "bottom";
				position = "80 7";
				extent = "110 20";
				minExtent = "8 2";
				enabled = "1";
				visible = "1";
				clipToParent = "1";
				color = "0 0 0 200";

				new GuiTextCtrl(vUF_name_2) {
					profile = "vUF_nameTextProfile";
					horizSizing = "right";
					vertSizing = "bottom";
					position = "0 0";
					extent = "110 20";
					minExtent = "8 2";
					enabled = "1";
					visible = "1";
					clipToParent = "1";
					text = %name;
					maxLength = "255";
				};
			};
	
			new guiBitMapCtrl(vUF_heartIcon_2)
			{
					profile = "GuiDefaultProfile";
					horizSizing = "right";
					vertSizing = "bottom";
					position = "80 30";
					extent = "16 16";
					minExtent = "8 2";
					enabled = "1";
					visible = "1";
					clipToParent = "1";
					bitmap = "./icons/heart.png";
					wrap = "0";
					lockAspectRatio = "0";
					alignLeft = "0";
					alignTop = "0";
					overflowImage = "0";
					keepCached = "0";
			};
	
			new GuiSwatchCtrl(vUF_healthbar_2) {
				profile = "GuiDefaultProfile";
				horizSizing = "left";
				vertSizing = "bottom";
				position = "98 30"; //80, 30, 110, 15
				extent = "92 15";
				minExtent = "94 9";
				visible = "1";
				color = "0 0 0 255";
	
				new GuiSwatchCtrl(vUF_healthMeter_2) {
					profile = "GuiDefaultProfile";
					horizSizing = "right";
					vertSizing = "bottom";
					position = "1 1";
					extent = "90 13";
					minExtent = "1 7";
					visible = "1";
					color = "0 255 0 255";
				};
			};
		};

		playGui.add(%a);
		//avatar stuff - thanks RTB!

		vUF_AV_2.setEmpty();
		vUF_AV_2.dynamicObject = "fffObject";
		vUF_AV_2.setObject(vUF_AV_2.dynamicObject, "base/data/shapes/player/m.dts", "", 0);

		vUF_AV_2.setSequence(vUF_AV_2.dynamicObject, 0, "headup", 0);
		vUF_AV_2.setSequence(vUF_AV_2.dynamicObject, 1, "run", 0.85);
		   
		vUF_AV_2.forceFOV = 15;
		vUF_AV_2.setOrbitDist(3);
		vUF_AV_2.setCameraRot(0.22,0.5,3.14);
		vUF_AV_2.lightDirection = "0 0.2 0.2";

		//have to do crazy stuff for this, don't know of another solution 
		%q = findFirstFile("Add-Ons/Face_*/" @ $vUF::target::FaceName @ ".png");

		if(%q !$= "")
		{
			%face = strReplace(%q, "thumbs/", "");
		}
		else //it'll error out and use a default picture, player doesn't have decal basically
		{
			%face = $vUF::target::FaceName;
		}

		%skincolor = $vUF::target::HeadColor;
		%hat = $vUF::target::Hat;
		%hatColor = $vUF::target::HatColor;
		%accent = $vUF::target::Accent;
		%accentColor = $vUF::target::AccentColor;
		%chest = $vUF::target::Chest;
		%chestColor = $vUF::target::TorsoColor;
		%arm = $vUF::target::LArm;
		%armColor = $vUF::target::LArmColor;

		//have to do crazy stuff for this, don't know of another solution 
		%p = findFirstFile("Add-Ons/Decal_*/" @ $vUF::target::DecalName @ ".png");

		if(%p !$= "")
		{
			%chestDecal = strReplace(%p, "thumbs/", "");
		}
		else //it'll error out and use a default picture
		{
			%chestDecal = $vUF::target::DecalName;
		}

		%pack = $vUF::target::Pack;
		%packColor = $vUF::target::PackColor;
		%secondPack = $vUF::target::SecondPack;
		%secondPackColor = $vUF::target::SecondPackColor;
		
		%i=0;
		while($face[%i] !$= "")
		{
		 	if($face[%i] $= %face)
		 	{
		 		%face = %i;
		 		break;
		 	}
		 	%i++;
		 	
		 	if(%i > 500)
		 		break;
		}
		
		%i=0;
		while($decal[%i] !$= "")
		{
			if($decal[%i] $= %chestDecal)
			{
			 	%chestDecal = %i;
			 	break;
			}
			%i++;
			
			if(%i > 500)
				break;
		}

		%parts = "accent hat chest pack secondpack larm rarm lhand rhand hip lleg rleg";
		for(%i=0;%i<getWordCount(%parts);%i++)
		{
			%k = 0;
			eval("%partName = $"@getWord(%parts,%i)@%k@";");
			while(%partName !$= "")
			{
				if(%partName !$= "none")
					vUF_AV_2.hidenode(vUF_AV_2.dynamicObject,%partName);
				eval("%partName = $"@getWord(%parts,%i)@%k++@";");
				%b++;
			}
		}
		
		vUF_AV_2.setNodeColor(vUF_AV_2.dynamicObject,"ALL",%skinColor);
		vUF_AV_2.setIFLFrame(vUF_AV_2.dynamicObject,"face",%face);
		vUF_AV_2.setIFLFrame(vUF_AV_2.dynamicObject,"decal",%chestDecal);
		
		if(%hat !$= "" && %hat !$= 0)
		{
			vUF_AV_2.unhidenode(vUF_AV_2.dynamicObject,$hat[%hat]);
			vUF_AV_2.setnodeColor(vUF_AV_2.dynamicObject,$hat[%hat],%hatColor);
			
			%accent = getWord($accentsAllowed[$hat[%hat]],%accent);
			if(%accent !$= "" && %accent !$= "none")
			{
				vUF_AV_2.unhidenode(vUF_AV_2.dynamicObject,%accent);
				vUF_AV_2.setnodeColor(vUF_AV_2.dynamicObject,%accent,%accentColor);
			}
		}
		
		if(%pack !$= "" && %pack !$= 0)
		{
			vUF_AV_2.unhidenode(vUF_AV_2.dynamicObject,$pack[%pack]);
			vUF_AV_2.setnodeColor(vUF_AV_2.dynamicObject,$pack[%pack],%packColor);
		}

		if(%secondpack !$= "" && %secondpack !$= 0)
		{
			vUF_AV_2.unhidenode(vUF_AV_2.dynamicObject,$secondpack[%secondpack]);
			vUF_AV_2.setnodeColor(vUF_AV_2.dynamicObject,$secondpack[%secondpack],%secondpackColor);
		}

		vUF_AV_2.unhidenode(vUF_AV_2.dynamicObject,$chest[%chest]);
		vUF_AV_2.setNodeColor(vUF_AV_2.dynamicObject,$chest[%chest],%chestColor);
		vUF_AV_2.unhidenode(vUF_AV_2.dynamicObject,$larm[%arm]);
		vUF_AV_2.setNodeColor(vUF_AV_2.dynamicObject,$larm[%arm],%armColor);
		vUF_AV_2.unhidenode(vUF_AV_2.dynamicObject,$rarm[%arm]);
		vUF_AV_2.setNodeColor(vUF_AV_2.dynamicObject,$rarm[%arm],%armColor);

		vUF_AV_2.hideNode(vUF_AV_2.dynamicObject,"lhand");
		vUF_AV_2.hideNode(vUF_AV_2.dynamicObject,"rhand");
	}
}

function clientCmdvUF_infoLoop()
{
	cancel( $vUF::infoLoop );

	if( vUF_main_self.visible )
	{
		vUF_requestInfo(0);

		$vUF::infoLoop = schedule( 1000 , 0 , clientCmdvUF_infoLoop );
	}
}

function vUF_requestInfo( %this )
{
	if( %this == 0 )
	{
		commandToServer( 'vUF_getInfo' , 0 );
	}
}

function clientCmdvUF_getAVFromServer(%type, %this)
{
	if( %type !$= "" && %this !$= "" )
	{
		eval("$vUF::target::" @ %type @ " = \"" @ %this @ "\";");
	}
}

function clientCmdvUF_pushtarget(%this, %name)
{
	if( %this && %name !$= "" )
	{
		clientCmdvUF_pushUF( "target" , %name);
	}
}

function clientCmdvUF_unPushtarget(%this)
{
	if( %this && isObject(vUF_main_2))
	{
		vUF_main_2.delete();
	}
}

function clientCmdvUF_recieveInfo( %type , %this )
{
	if( %type $= "self" )
	{
		//thanks Iban

		// The client command takes the amount of damage.
		// We want to show the amount of health, so we need to invert the number.
		%percent = 1 - %this;
		
		if(%percent > 0)
		{
			if( vUF_deathImage_self.visible )
			{
				vUF_deathImage_self.setVisible(0);
			}

			if(%percent <= 1)
			{
				
				%color = "0 255 0 255";
			}
			else
			{
				vUF_deathImage_self.setVisible(1);
			}
			
			vUF_healthMeter_self.extent = mFloor(90 * %percent) SPC getWord(vUF_healthMeter_self.extent, 1); // Width = maxExtent x currentHP
			vUF_healthMeter_self.color = %color;
		}
		else
		{
			//THEY ARE DEAD
			vUF_deathImage_self.setVisible(1);

			%color = "0 0 0 255";
			vUF_healthMeter_self.extent = mFloor(90 * %percent) SPC getWord(vUF_healthMeter_self.extent, 1); // Width = maxExtent x currentHP
			vUF_healthMeter_self.color = %color;
		}
	}
	else if( %type $= "target" )
	{
		//thanks Iban

		// The client command takes the amount of damage.
		// We want to show the amount of health, so we need to invert the number.
		%percent = 1 - %this;
		
		if(%percent > 0)
		{
			if( vUF_deathImage_2.visible )
			{
				vUF_deathImage_2.setVisible(0);
			}

			if(%percent <= 1)
			{	
				%color = "0 255 0 255";
			}
			else
			{
				// If we have more than 100%, go into white mode.
				%color = "255 255 255 255";
				vUF_deathImage_2.setVisible(1);
			}
			
			vUF_healthMeter_2.extent = mFloor(90 * %percent) SPC getWord(vUF_healthMeter_2.extent, 1); // Width = maxExtent x currentHP
			vUF_healthMeter_2.color = %color;
		}
		else
		{
			//THEY ARE DEAD
			vUF_deathImage_2.setVisible(1);

			%color = "0 0 0 255";
			vUF_healthMeter_2.extent = mFloor(90 * %percent) SPC getWord(vUF_healthMeter_2.extent, 1); // Width = maxExtent x currentHP
			vUF_healthMeter_2.color = %color;
		}
	}
}

package vUF_client
{
	function useBricks(%this)
	{
		if(%this && $vUF::topper == 0)
		{
			if(isObject(vUF_main_self))
			{
				vUF_main_self.position = (getWord(getRes(),0)/2)-275 SPC getWord(getRes(),1)-170;

				if(isObject(vUF_main_2))
				{
					vUF_main_2.position = (getWord(getRes(),0)/2)+75 SPC getWord(getRes(),1)-170;
				}

				$vUF::topper = 1;
			}
		}
		else if(%this && $vUF::topper)
		{
			if(isObject(vUF_main_self))
			{
				vUF_main_self.position = (getWord(getRes(),0)/2)-275 SPC getWord(getRes(),1)-170+60;

				if(isObject(vUF_main_2))
				{
					vUF_main_2.position = (getWord(getRes(),0)/2)+75 SPC getWord(getRes(),1)-170+60;
				}

				$vUF::topper = 0;
			}
		}

		parent::useBricks(%this);
	}
};
activatePackage(vUF_client);

package vUF_client
{
	function tmbi_toggle()
	{
		parent::tmbi_toggle();

		if(%this && $vUF::topper == 0)
		{
			if(isObject(vUF_main_self))
			{
				vUF_main_self.position = (getWord(getRes(),0)/2)-275 SPC getWord(getRes(),1)-170;

				if(isObject(vUF_main_2))
				{
					vUF_main_2.position = (getWord(getRes(),0)/2)+75 SPC getWord(getRes(),1)-170;
				}

				$vUF::topper = 1;
			}
		}
		else if(%this && $vUF::topper)
		{
			if(isObject(vUF_main_self))
			{
				vUF_main_self.position = (getWord(getRes(),0)/2)-275 SPC getWord(getRes(),1)-170+60;

				if(isObject(vUF_main_2))
				{
					vUF_main_2.position = (getWord(getRes(),0)/2)+75 SPC getWord(getRes(),1)-170+60;
				}

				$vUF::topper = 0;
			}
		}
	}

	function useBricks(%this)
	{
		if(%this && $vUF::topper == 0)
		{
			if(isObject(vUF_main_self))
			{
				vUF_main_self.position = (getWord(getRes(),0)/2)-275 SPC getWord(getRes(),1)-170;

				if(isObject(vUF_main_2))
				{
					vUF_main_2.position = (getWord(getRes(),0)/2)+75 SPC getWord(getRes(),1)-170;
				}

				$vUF::topper = 1;
			}
		}
		else if(%this && $vUF::topper)
		{
			if(isObject(vUF_main_self))
			{
				vUF_main_self.position = (getWord(getRes(),0)/2)-275 SPC getWord(getRes(),1)-170+60;

				if(isObject(vUF_main_2))
				{
					vUF_main_2.position = (getWord(getRes(),0)/2)+75 SPC getWord(getRes(),1)-170+60;
				}

				$vUF::topper = 0;
			}
		}

		parent::useBricks(%this);
	}
};
activatePackage(vUF_client);


//get ifl frame, by Iban

function getIflFrame(%node, %name)
{
	%file = "base/data/shapes/player/" @ %node @ ".ifl";
	
	if(isFile(%file))
	{
		%name = stripTrailingSpaces(fileBase(%name));
		
		if(%name !$= "")
		{
			%fo = new FileObject() {};
			%fo.openForRead(%file);
			
			%hasFound = false;
			%lineNum = 0;
			
			while(!%fo.isEOF())
			{
				%line = %fo.readLine();
				%thisDecal = stripTrailingSpaces(fileBase(%line));
				
				if(%thisDecal $= %name)
				{
					%hasFound = true;
					break;
				}
				else
				{
					%lineNum++;
				}
			}
			
			%fo.close();
			%fo.delete();
			
			if(%hasFound)
			{
				return %lineNum;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}
	else
	{
		echo("\c2getIflFrame(\"" @ %node @ "\", \"" @ %name @ "\") : You did not supply a proper IFL file name to read from.");
		return 0;
	}
}