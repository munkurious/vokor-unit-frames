//Vokor Unit Frames
//By Vokor 
// - Munkurious : Lead Developer
// - Iban : Code for Health Bar
// - Ephi : Code for avatar in Object View

//server.cs

//GUI Registering

package vUF_guiRegister
{
	function GameConnection::onClientEnterGame( %this )
	{
		parent::onClientEnterGame( %this );

		commandToClient( %this , 'vUF_registerOne');
	}
};

activatePackage(vUF_guiRegister);

function serverCmdvUF_registerTwo( %this , %bool )
{
	if( %bool )
	{
		%this.vUF_hasGui = 1;

		warn("vUF : " @ %this.name SPC "has vUF client.");

		//tell to open Client Unit Frame
		%name = %this.name;
		commandToClient( %this , 'vUF_pushUF' , "SELF" , %name );
	}
}

function serverCmdvUF_getInfo( %client , %this )
{
	if( %client.vUF_hasGui )
	{
		if( %this == 0 ) //not sure why I am double checking this but whatever
		{
			%player = %client.player;

			if( isObject( %player ) )
			{
				%dmgPercent = %player.getDamageLevel();
				%dmgPercent = %dmgPercent / %player.getDatablock().maxDamage;
			}
			else
			{
				%dmgPercent = 1;
			}
			schedule(10, 0, commandToClient, %client , 'vUF_recieveInfo' , "self" , %dmgPercent );

			if(%client.target !$= "")
			{
				%player = %client.target.player;

				if( isObject( %player ) )
				{
					%dmgPercent = %player.getDamageLevel();
					%dmgPercent = %dmgPercent / %player.getDatablock().maxDamage;
				}
				else
				{
					%dmgPercent = 1;
				}
				schedule(20, 0, commandToClient, %client , 'vUF_recieveInfo' , "target" , %dmgPercent );
			}
		}
	}
}

function vUF_target(%this, %target)
{
	%cl = findClientByName(%target);

	if(%cl == %this)
	{
		//don't
		return;
	}

	if(isObject(%this.target))
	{
		%this.target.targetNumber--;
		schedule(200, 0, commandToClient, %this, 'vUF_unPushtarget', 1);
	}

	if(isObject(%cl.player))
	{
		%this.target = %cl;
		%this.target.targetNumber++;
	
		//lets send the avatar stuff down! because avatar has so much info, if they change their avatar
		//it won't update client sided unless they retarget

		schedule(0, 0, commandToClient, %this, 'vUF_getAvFromServer', "FaceName", %cl.faceName);
		schedule(10, 0, commandToClient, %this, 'vUF_getAvFromServer', "HeadColor", %cl.headColor);
		schedule(20, 0, commandToClient, %this, 'vUF_getAvFromServer', "Hat", %cl.hat);
		schedule(30, 0, commandToClient, %this, 'vUF_getAvFromServer', "HatColor", %cl.hatcolor);
		schedule(40, 0, commandToClient, %this, 'vUF_getAvFromServer', "Accent", %cl.accent);
		schedule(50, 0, commandToClient, %this, 'vUF_getAvFromServer', "AccentColor", %cl.accentColor);
		schedule(60, 0, commandToClient, %this, 'vUF_getAvFromServer', "Chest", %cl.chest);
		schedule(70, 0, commandToClient, %this, 'vUF_getAvFromServer', "TorsoColor", %cl.ChestColor);
		schedule(80, 0, commandToClient, %this, 'vUF_getAvFromServer', "LArm", %cl.Larm);
		schedule(90, 0, commandToClient, %this, 'vUF_getAvFromServer', "LarmColor", %cl.lArmColor);
		schedule(100, 0, commandToClient, %this, 'vUF_getAvFromServer', "DecalName", %cl.DecalName);
		schedule(110, 0, commandToClient, %this, 'vUF_getAvFromServer', "Pack", %cl.pack);
		schedule(120, 0, commandToClient, %this, 'vUF_getAvFromServer', "PackColor", %cl.packColor);
		schedule(130, 0, commandToClient, %this, 'vUF_getAvFromServer', "SecondPack", %cl.secondPack);
		schedule(140, 0, commandToClient, %this, 'vUF_getAvFromServer', "SecondPackColor", %cl.secondPackColor);

		//now that it has the avatar prefs, let's push it!

		schedule(200, 0, commandToClient, %this, 'vUF_pushtarget', 1, %target);
		//schedule(250, 0, messageClient, %this, "\c6target has been set to \c3" @ %cl.name @ "\c6.");

		%this.vUF_lasttargetTime = getSimTime();
	}
}

function serverCmdUntarget(%this)
{
	if(isObject(%this))
	{
		if(%this.target !$= "")
		{
			%this.target.targetNumber--;
			commandToClient(%this, 'vUF_unPushtarget', 1);
			%this.target = "";
		}
	}
}

//originally going to have a party system for this, but after lots of arguing
//with myself, I decided it was not the best idea for mass Blockland servers,
//would only truly be useful on specific servers.

//instead, we will simply be able to target a player, giving only health info
//also, combat text ! (at least about yourself) 

function GameConnection::vUF_infoLoop(%this)
{
	if(isObject(%this) && %this.vUF_hasGui)
	{
		serverCmdvUF_getInfo( %this, 0 );

		cancel(%this.vUF_loop);

		%this.vUF_loop = %this.schedule(500, vUF_infoLoop);
	}
}

function GameConnection::vUF_setTarget(%this, %target)
{
	if(isObject(%this) && %this.vUF_hasGui && isObject(findClientByName(%target).player))
	{
		if(%this.vUF_lasttargetTime $= "")
		{
			%this.vUF_lasttargetTime = 0;
		}

		//talk(%this.name SPC %this SPC isObject(%this.target.player) SPC %this.target.name SPC %this);
		if((getSimTime() - %this.vUF_lasttargetTime) <= 5000)
		{
			if(isObject(%this.target.player))
			{
				//do nothing!
			}
			else
			{
				vUF_target(%this, %target);
			}
		}
		else
		{
			vUF_target(%this, %target);
		}
	}
}

package vUF
{
	function armor::Damage(%this, %obj, %sourceObject, %position, %damage, %damageType)
	{
		parent::Damage(%this, %obj, %sourceObject, %position, %damage, %damageType);

		%obj.client.vUF_infoLoop();

		%cl = %sourceObject.client;

		%cl.vUF_setTarget(%obj.client.name);

		%sourceObject.client.vUF_infoLoop();
	}

	function GameConnection::onClientEnterGame( %this )
	{
		parent::onClientEnterGame( %this );

		%this.vUF_infoLoop();
	}
};

activatePackage(vUF);